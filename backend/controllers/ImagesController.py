from flask_restful import Resource
from flask import jsonify, request
from services.ImagesService import ImagesService
import os
import PIL.Image
from utilities import ImageConverter
from utilities.Page import Page
from mappers.ImageMapper import ImageMapper
import json

class ImagesController(Resource):
    
    def get(self):
        pageNumber = request.args.get("page", 1, type=int)
        pageSize = request.args.get("per-page", 5, type=int)
        page = Page(pageNumber, pageSize)
        data = self._imagesService.Get(page)
        responseBody = ImageMapper().ToResponseWithPagination(data)
        return responseBody
    
    def post(self):
        image = request.files['image']
        name: str = request.form.get('name', None)
        image = PIL.Image.open(image.stream)
        try:
            imageEntity = ImagesService().Insert(image, name)
        except:
            return {"message": "Bad request"}, 400
        if not image:
            return {'message': 'Git gud'}
        responseBody = ImageMapper().ToResponse(imageEntity)
        return responseBody

    def __init__(self):
        self._imagesService: ImagesService = ImagesService()
        
class ImagesByUrlController(Resource):
    
    def post(self):
        jsonData = request.get_json()
        name = jsonData['name']
        url = jsonData['url']
        if not url:
            return {"message": "Bad request"}, 400
        try:
            imageEntity = ImagesService().InsertImageByUrl(url, name)
        except:
            return {"message": "Bad request"}, 400
        responseBody = ImageMapper().ToResponse(imageEntity)
        return responseBody

    def __init__(self):
        self._imagesService: ImagesService = ImagesService()
        
class ImageController(Resource):
    def get(self, id: int):
        data = self._imagesService.GetById(id)
        if not data:
            return {"message": "Image not found"}, 404
        responseBody = ImageMapper().ToResponse(data)
        return responseBody
    
    def put(self, id: int):
        jsonData = request.get_json()
        name = jsonData['name']
        if not name:
            return {"message": "Bad request"}, 400
        self._imagesService.UpdateImageName(id, name)
        return '', 204
    
    def delete(self, id: int):
        self._imagesService.RemoveById(id)
        return '', 204

    def __init__(self):
        self._imagesService: ImagesService = ImagesService()
    