import os

from email.mime import base
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from repositories.ImagesRepository import ImagesRepository
from services.ImagesService import ImagesService
from controllers.ImagesController import ImagesController, ImageController, ImagesByUrlController
from os import environ
from database import db
import torch

basedir = os.path.abspath(os.path.dirname(__file__))
print("basedir = ", basedir)
application = Flask("tricky-search")
# application.config['SQLALCHEMY_DATABASE_URI'] = environ['DATABASE_URL']
print(basedir)
application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db/database.db')
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
api = Api(application)
# db = SQLAlchemy(application)
db.init_app(application)

# imagesRepository = ImagesRepository()
# imagesService = ImagesService(imagesRepository=imagesRepository)
# imagesController = ImagesController(imagesService=imagesService)

api.add_resource(ImagesController, '/images')
api.add_resource(ImagesByUrlController, '/images/url')
api.add_resource(ImageController, '/images/<int:id>', '/images/<int:id>/name')

with application.app_context():
    db.create_all()
    application.run(debug=False)
# application.run(debug=True)
