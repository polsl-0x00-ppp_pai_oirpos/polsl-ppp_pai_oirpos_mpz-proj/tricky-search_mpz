import os
import torch
import PIL.Image
import numpy as np

basedir = os.path.abspath(os.path.dirname(__file__))
model = torch.hub.load('ultralytics/yolov5', 'custom', path= basedir + '/best.pt')

class ImageRecognizer():
    def DetectObjects(self, image : PIL.Image):        
        if not image:
            return None
        results = model(image)
        extractedImageAsNumpyArray = results.render()[0]
        imageAfterRecognition = PIL.Image.fromarray(np.uint8(extractedImageAsNumpyArray)).convert('RGB')
        categoriesPandasArray = list()
        try:
            categoriesPandasArray = results.pandas().xyxy[0]['name']
        except:
            pass
        categories = list()
        for name in categoriesPandasArray:
            categories.append(name)
        categories = list(set(categories))
        return (imageAfterRecognition, categories)
