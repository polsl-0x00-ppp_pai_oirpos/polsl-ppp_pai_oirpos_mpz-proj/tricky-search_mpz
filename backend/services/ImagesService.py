from repositories.ImagesRepository import ImagesRepository
import io
import PIL.Image
from models.Image import DBImage
from ai.ImageRecognizer import ImageRecognizer
from utilities import ImageConverter
import json
import requests
from io import BytesIO

class ImagesService:
    def Get(self, page):
        entities = self._imagesRepository.GetPage(page)
        return entities
    
    def GetById(self, id: int):
        entity = self._imagesRepository.GetById(id)
        return entity
    
    def Insert(self, image: PIL.Image, name = None) -> DBImage:
        return self.__insert(image, name, None)
    
    def InsertImageByUrl(self, url: str, name: str):
        
        fileData = BytesIO(requests.get(url, timeout=3).content)
        image = PIL.Image.open(fileData)
        
        return self.__insert(image, name, url)
    
    def UpdateImageName(self, id, name):
        self._imagesRepository.UpdateName(id, name)
        
    def RemoveById(self, id):
        self._imagesRepository.RemoveById(id)
        
    def __insert(self, image: PIL.Image, name, sourceUrl) -> DBImage:
        resultImage, categories = ImageRecognizer().DetectObjects(image)
        
        entity = DBImage()
        entity.sourceUrl = sourceUrl
        entity.sourceImageType = image.format
        entity.sourceImageData = ImageConverter.ToBytes(image)
        bytesBuffer = io.BytesIO()
        resultImage.save(bytesBuffer, format=image.format)
        entity.resultImageType = image.format
        entity.resultImageData = bytesBuffer.getvalue()
        entity.name = name
        entity.categories = json.dumps(categories)
        
        self._imagesRepository.Insert(entity)
        return entity
    
    def __init__(self):
        self._imagesRepository: ImagesRepository = ImagesRepository()