import os
from models.Image import DBImage
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from database import db
import PIL.Image
import io

class ImagesRepository():
    def GetPage(self, page):
        if page.size < 1 or page.size > 100:
            page.size = 10
        return DBImage.query.order_by(DBImage.id.desc()).paginate(page=page.no, per_page=page.size)
    
    def GetById(self, id: int):
        return DBImage.query.get(id)
    
    def Insert(self, entity: DBImage):
        db.session.add(entity)
        db.session.commit()
        
    def UpdateName(self, id: int, name: str):
        db.session.query(DBImage).filter(DBImage.id == id).update({'name': name})
        db.session.commit()
        
    def RemoveById(self, id: int):
        DBImage.query.filter_by(id=id).delete()
        db.session.commit()
    