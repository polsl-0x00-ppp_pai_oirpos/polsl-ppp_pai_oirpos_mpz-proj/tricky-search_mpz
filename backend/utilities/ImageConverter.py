import io
import PIL.Image
import base64

def ToBytes(image) -> bytes:
    bytesBuffer = io.BytesIO()
    image.save(bytesBuffer, format=image.format)
    return bytesBuffer.getvalue()

def ToBase64(image) -> str:
    if type(image) is not bytes:
        imageRaw = ToBytes(image)
    else:
        imageRaw = image
    return base64.b64encode(imageRaw).decode()