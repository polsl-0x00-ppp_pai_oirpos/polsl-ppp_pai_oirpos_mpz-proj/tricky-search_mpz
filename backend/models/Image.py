from flask_sqlalchemy import SQLAlchemy
from database import db
from sqlalchemy.dialects.sqlite import BLOB

class DBImage(db.Model):
    __tablename__ = "images"
 
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    sourceUrl = db.Column(db.String)
    sourceImageType = db.Column(db.String, nullable = False)
    sourceImageData = db.Column(BLOB, nullable = False)
    resultImageType = db.Column(db.String, nullable = False)
    resultImageData = db.Column(BLOB, nullable = False)
    categories = db.Column(db.String, nullable = False)
