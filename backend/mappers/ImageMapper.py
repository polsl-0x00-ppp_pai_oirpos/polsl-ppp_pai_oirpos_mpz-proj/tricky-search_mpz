from models.Image import DBImage
from utilities import ImageConverter
import json

class ImageMapper():
    def ToResponseWithPagination(self, imageEntities):
        results = {
        "results": [self.ToResponse(entity) for entity in imageEntities.items],
        "pagination": {
            "count": imageEntities.total,
            "page": imageEntities.page,
            "per_page": imageEntities.per_page,
            "pages": imageEntities.pages,
        },
        }
        for result in results["results"]:
            result.pop('imageData', None)
        return results
    
    def ToResponse(self, imageEntity: DBImage):
        return {
                       'id': imageEntity.id,
                       'name': imageEntity.name,
                       'sourceUrl': imageEntity.sourceUrl,
                       'imageType': imageEntity.resultImageType,
                       'imageData': ImageConverter.ToBase64(imageEntity.resultImageData),
                       'categories': json.loads(imageEntity.categories)
                       };