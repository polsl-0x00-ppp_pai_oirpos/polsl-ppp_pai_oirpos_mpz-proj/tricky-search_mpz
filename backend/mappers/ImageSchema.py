from marshmallow import Schema, fields, ValidationError, pre_load

class ImageSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    sourceUrl = fields.String()
    sourceImageType = fields.String()
    sourceImageData = fields.Raw()
    resultImageType = fields.String()
    resultImageData = fields.Raw()
    categories = fields.String()

# Example validator
def NotBlank(data):
    if not data:
        raise ValidationError('Value must be not blank')
