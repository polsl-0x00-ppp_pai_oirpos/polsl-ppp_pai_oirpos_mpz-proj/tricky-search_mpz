# tricky-search

## Opis
Projekt umożliwia rozpoznawanie obiektów na obrazkach przy pomocy sieci YOLO.  
Projekt składa się z dwóch aplikacji:
- backend - Python/Flash/SQLAlchemy - udostępniającej REST API udostępniającej interfejs typu CRUD dla obrazków.
- frontend - Next.js - udostępniającej przykładowy interfejs graficzny (typu aplikacji webowej). 

Oprócz tego w projekcie znajduje się plik docker-compose umożliwiający (po zainstalowaniu środowiska docker) automatycznie zbudowanie aplikacji, opakowanie jej w obraz dockerowy oraz uruchomienia. 
Domyślnie zosatją uruchomione trzy kontenery:
- tricky-search-backend - port 5000 - zawiera aplikacje backendową.
- tricky-search-froncik - port 3000 - zawiera aplikacje frontendową.
- tricky-search-openapi - port 9000 - udostępnia graficzną reprentacje dokumentacji API (OpenAPI/Swagger).


## Uruchamianie aplikacji
#### Zalecane jest uruchomienie aplikacji za pośrednictwem docker-compose
### Docker
Poniższa komenda uruchomi kontener w ramach bieżącej sesji.
```shell
docker compose up --build
```
### W konsoli - shell
Uruchomić pliki `run.sh` dostępne w katalogach frontend i backend w zależności od wykorzystywanej aplikacji

## Development
### Backend
Do developmentu aplikacji należy uruchomić edytor Visual Studio Code (z zainstalowanymi i działającymi wtyczkami do języka Python) z poziomu folderu backend. 
Aplikacje należy uruchamiać/debuggować z poziomu pliku `app.py`. 