import UploadByUrl from "@/components/UploadManager/UploadByUrl";
import FileContextProvider from "@/context/fileContext";

const UploadByUrlPage = () => (
  <FileContextProvider>
    <UploadByUrl />
  </FileContextProvider>
);

export default UploadByUrlPage;
