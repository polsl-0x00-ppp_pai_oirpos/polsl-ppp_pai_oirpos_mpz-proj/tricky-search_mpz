import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { QueryClient, QueryClientProvider } from "react-query";
import Navbar from "@/components/Navbar";

const client = new QueryClient();
export default function App({ Component, pageProps }: AppProps) {
  return (
    <QueryClientProvider client={client}>
      <Navbar />
      <main className="mt-[60px]">
        <Component {...pageProps} />
      </main>
    </QueryClientProvider>
  );
}
