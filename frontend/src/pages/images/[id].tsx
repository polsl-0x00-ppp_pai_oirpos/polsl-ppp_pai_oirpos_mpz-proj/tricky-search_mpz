import QueryScreen from "@/components/QueryScreen";
import { useRouter } from "next/router";

const ImageViewPage = () => {
  const { query } = useRouter();

  return <QueryScreen id={Number(query.id) as number} />;
};

export default ImageViewPage;
