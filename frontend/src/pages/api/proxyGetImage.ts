import { IdentifyImageResponse } from "@/types";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IdentifyImageResponse>
) {
  const { id } = req.query;

  const result = await fetch(`http://tricky-search-backend:5000/images/${id}`, {
    method: "GET",
  });
  const body = await result.json();
  return res.status(result.status).json(body);
}
