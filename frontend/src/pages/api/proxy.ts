import { IdentifyImageResponse } from "@/types";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IdentifyImageResponse>
) {
  const buffers: any[] = [];
  req.on("readable", () => {
    const chunk = req.read();
    if (chunk !== null) {
      buffers.push(chunk);
    }
  });

  req.on("end", async () => {
    const result = await fetch("http://tricky-search-backend:5000/images", {
      method: "POST",
      body: Buffer.concat(buffers),
      headers: {
        "Content-Type": req.headers["content-type"] ?? "multipart/form-data",
      },
    });
    const body = await result.json();
    return res.status(result.status).json(body);
  });
}
export const config = {
  api: {
    bodyParser: false,
  },
};
