import { IdentifyImageResponse } from "@/types";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IdentifyImageResponse>
) {
  const { page, perPage } = req.query;

  const result = await fetch(
    `http://tricky-search-backend:5000/images?page=${page}&per-page=${perPage}`,
    {
      method: "GET",
    }
  );
  const body = await result.json();
  return res.status(result.status).json(body);
}
