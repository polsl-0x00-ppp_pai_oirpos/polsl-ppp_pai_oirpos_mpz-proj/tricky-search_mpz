import UploadByFile from "@/components/UploadManager/UploadByFile";
import FileContextProvider from "@/context/fileContext";

const UploadPage = () => {
  return (
    <FileContextProvider>
      <UploadByFile />
    </FileContextProvider>
  );
};

export default UploadPage;
