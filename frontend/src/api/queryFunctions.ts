import { FileUrl } from "@/context/fileContext";
import { IdentifyImageResponse } from "@/types";

export const postImageFile = async (
  file: File
): Promise<IdentifyImageResponse> => {
  const formData = new FormData();

  formData.append("image", file);
  const res = await fetch("http://localhost:3000/api/proxy", {
    method: "POST",
    body: formData,
  });
  return await res.json();
};

export const postImageUrl = async (
  file: FileUrl
): Promise<IdentifyImageResponse> => {
  const res = await fetch("http://localhost:3000/api/proxyUrl", {
    method: "POST",
    body: JSON.stringify(file),
  });
  if (res.status !== 200) throw new Error("Error while fetching image data");
  return await res.json();
};
