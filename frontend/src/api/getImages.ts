import { IdentifyImageResponse } from "@/types";
import { useInfiniteQuery, UseInfiniteQueryResult } from "react-query";

const YOLO_KEY = "yolo_images";
const PER_PAGE = 4;
export const useAllImages = (): UseInfiniteQueryResult<Response> =>
  useInfiniteQuery([YOLO_KEY], ({ pageParam = 1 }) => getImages(pageParam), {
    getNextPageParam,
  });

type Response = {
  pagination: {
    page: number;
    pages: number;
  };
  results: IdentifyImageResponse;
};
export const getNextPageParam = (
  lastPage: Response,
  allPages: Response[]
): number | null => {
  const {
    pagination: { pages, page },
  } = lastPage;
  if (pages === page) return null;
  return page + 1;
};

const getImages = async (pageParam: number): Promise<Response> => {
  const res = await fetch(
    `http://localhost:3000/api/proxyImages?page=${pageParam}&perPage=${PER_PAGE}`,
    {
      method: "GET",
    }
  );
  return await res.json();
};
