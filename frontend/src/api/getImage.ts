import { IdentifyImageResponse } from "@/types";
import { useQuery, UseQueryResult } from "react-query";

const YOLO_KEY = "yolo_get_image";
export const useYoloImage = (
  id: number
): UseQueryResult<IdentifyImageResponse> =>
  useQuery([YOLO_KEY, id], () => getImage(id), {});

const getImage = async (id: number) => {
  const res = await fetch(`http://localhost:3000/api/proxyGetImage?id=${id}`, {
    method: "GET",
  });
  const body = (await res.json()) as IdentifyImageResponse;
  return body;
};
