import { IdentifyImageResponse } from "@/types";
import { FC } from "react";

type Props = IdentifyImageResponse;

const FileTile: FC<Props> = ({ imageData, categories }) => {
  return (
    <>
      <img width={800} src={`data:image/jpg;base64, ${imageData}`} />
      <div className="flex flex-col gap-4">
        <h2 className="text-orange text-3xl mt-4">Recognized objects: </h2>
        <ul className="text-white text-start self-start ">
          {categories.map((category) => (
            <li key={category} className="text-orange text-xl">
              {category}
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default FileTile;
