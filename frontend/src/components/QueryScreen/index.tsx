import Image from "next/image";
import ImageIcon from "public/image-line.svg";
import FileTile from "@/components/FileTile";
import ErrorScreen from "@/components/ErrorScreen";
import { FC } from "react";
import { useYoloImage } from "@/api/getImage";

type Props = {
  id: number;
};
const QueryScreen: FC<Props> = ({ id }) => {
  const { data, isLoading, isError } = useYoloImage(id);

  if (isLoading || !data)
    return (
      <div className="animate-pulse flex flex-col items-center">
        <Image src={ImageIcon} alt={"image loader"} width={120} height={120} />
        <h2 className="text-white text-2xl">Loading image...</h2>
      </div>
    );

  if (isError) return <ErrorScreen />;

  return (
    <div className="flex flex-col items-center px-10 mt-40">
      <h2 className="text-white text-2xl">YOLO result: </h2>
      <FileTile {...data} />
    </div>
  );
};

export default QueryScreen;
