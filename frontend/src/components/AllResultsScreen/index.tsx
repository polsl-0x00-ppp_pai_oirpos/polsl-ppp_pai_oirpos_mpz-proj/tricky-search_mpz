import FileTile from "@/components/FileTile";
import Loader from "@/components/Loader";
import ErrorScreen from "@/components/ErrorScreen";
import { useAllImages } from "@/api/getImages";

const AllResultsScreen = () => {
  const {
    data,
    isLoading,
    fetchNextPage,
    isError,
    isFetchingNextPage,
    hasNextPage,
  } = useAllImages();

  if (isError) return <ErrorScreen />;
  if (isLoading || !data) return <Loader />;
  const results = data.pages.flatMap((page) => page.results);

  return (
    <div className="flex flex-col items-center gap-10 mt-12">
      {results.map((result) => (
        <div className="border-orange bg-darker-800 border-4 p-10">
          <p className="text-orange text-2xl">{result.name}</p>
          <FileTile {...result} />
        </div>
      ))}
      {isFetchingNextPage && <Loader />}
      {hasNextPage && (
        <button
          onClick={() => fetchNextPage()}
          className="text-dark font-bold bg-orange px-[24px] py-[12px] rounded-[24px] mb-10"
        >
          Fetch more
        </button>
      )}
    </div>
  );
};

export default AllResultsScreen;
