import ErrorIcon from "public/folder-outline-alerted.svg";
import Image from "next/image";

const ErrorScreen = () => (
  <div className="flex flex-col items-center justify-center mx-5">
    <Image src={ErrorIcon} alt={"error folder"} width={120} height={120} />
    <h2 className="text-red sm:text-4xl text-3xl max-w-[600px] mt-4">
      Error occurred while requesting YOLO network. Validate provided image or
      try again later
    </h2>
  </div>
);

export default ErrorScreen;
