import { FILE_INPUT_ID } from "@/consts/elementIds";

const UploadButton = () => {
  return (
    <button
      className="text-dark font-bold bg-orange px-[24px] py-[12px] rounded-[24px] mt-12"
      onClick={() => document.getElementById(FILE_INPUT_ID)!.click()}
    >
      Upload file
    </button>
  );
};

export default UploadButton;
