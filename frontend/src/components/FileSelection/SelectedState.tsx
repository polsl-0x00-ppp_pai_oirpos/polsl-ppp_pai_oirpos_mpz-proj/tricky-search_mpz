import { FC } from "react";
import Image from "next/image";
import ImageIcon from "public/image-line.svg";
import { FILE_INPUT_ID } from "@/consts/elementIds";
import ChangeButton from "@/components/FileSelection/ChangeButton";

type Props = {
  fileName: string;
  handleFileSelection: (fileList: FileList) => void;
};

const SelectedState: FC<Props> = ({ fileName, handleFileSelection }) => {
  return (
    <div className="flex flex-col items-center">
      <Image src={ImageIcon} alt={"image"} width={80} height={80} />
      <p className="text-white text-2xl font-bold">Selected file: </p>
      <p className="text-xl font-bold text-purple italic mt-4">{fileName}</p>

      <div className="border-orange border w-full w-11/12 mt-6" />

      <input
        onChange={(event) => handleFileSelection(event.target.files!)}
        id={FILE_INPUT_ID}
        className="text-white hidden"
        type="file"
      />
      <ChangeButton />
    </div>
  );
};

export default SelectedState;
