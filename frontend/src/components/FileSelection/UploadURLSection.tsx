import { FileUrl } from "@/context/fileContext";
import { FC, useState } from "react";
import clsx from "clsx";

type Props = {
  onUpload: (fileUrl: FileUrl) => void;
};
const UploadURLSection: FC<Props> = ({ onUpload }) => {
  const [text, setText] = useState("");
  const [name, setName] = useState("");

  const handleSubmit = () => {
    onUpload({ url: text, name });
  };

  return (
    <>
      <div className="flex flex-col mt-10 mx-4">
        <h2 className="text-white text-2xl mb-2">Paste image URL here:</h2>
        <input
          className="bg-darker-600 text-white"
          value={text}
          onChange={(e) => setText(e.target.value)}
          type="url"
        />
        <h2 className="text-white text-2xl mb-2 mt-4">Name for your image: </h2>
        <input
          className="bg-darker-600 text-white"
          value={name}
          onChange={(e) => setName(e.target.value)}
          type="url"
        />
        <button
          onClick={handleSubmit}
          className={clsx(
            '"text-dark font-bold px-[24px] py-[12px] rounded-[24px] mt-4',
            {
              "bg-orange": text.length > 0 && name.length > 0,
              "bg-gray-700 border border-black":
                text.length === 0 || name.length === 0,
            }
          )}
        >
          Submit
        </button>
      </div>
    </>
  );
};
export default UploadURLSection;
