import { FILE_INPUT_ID } from "@/consts/elementIds";

const ChangeButton = () => {
  return (
    <button
      className="text-dark font-bold bg-orange px-[24px] py-[12px] rounded-[24px] mt-6"
      onClick={() => document.getElementById(FILE_INPUT_ID)!.click()}
    >
      Choose different file
    </button>
  );
};

export default ChangeButton;
