import { DragEvent, useState } from "react";
import clsx from "clsx";
import UploadStateManager from "@/components/FileSelection/UploadStateManager";
import { useFileContext } from "@/context/fileContext";

const ACCEPTED_MIME_TYPES = ["image/jpg", "image/png", "image/jpeg"];

export type UploadState = "Idle" | "Error" | "Selected" | "Dragging";

const DragZoneHandler = () => {
  const [uploadState, setUploadState] = useState<UploadState>("Idle");
  const [fileName, setFileName] = useState<null | string>(null);
  const { updateFile } = useFileContext();
  const dragOverHandler = (event: DragEvent) => {
    event.preventDefault();
    setUploadState("Dragging");
  };
  const dragLeaveHandler = () => {
    setUploadState("Idle");
  };

  const handleFileSelection = (fileList: FileList): void => {
    if (fileList.length !== 1) return setUploadState("Error");
    const file = fileList[0];
    if (!ACCEPTED_MIME_TYPES.includes(file.type)) {
      setUploadState("Error");
      return;
    }
    updateFile(file);
    setFileName(file.name);
    setUploadState("Selected");
  };

  const dropHandler = (event: DragEvent) => {
    event.preventDefault();
    handleFileSelection(event.dataTransfer.files);
  };

  return (
    <div
      onDrop={dropHandler}
      onDragLeave={dragLeaveHandler}
      onDragOver={dragOverHandler}
      className={clsx(
        "w-[350px] mx-12 bg-dark duration-500 transition-colors h-[300px] border border-orange bg-darker-800 rounded-3xl flex flex-col items-center pt-4 border-dashed",
        {
          "bg-dark-600": uploadState === "Dragging",
        }
      )}
    >
      <UploadStateManager
        fileName={fileName}
        uploadState={uploadState}
        handleFileSelection={handleFileSelection}
      />
    </div>
  );
};

export default DragZoneHandler;
