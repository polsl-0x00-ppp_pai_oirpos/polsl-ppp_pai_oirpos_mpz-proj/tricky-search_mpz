import Image from "next/image";
import ErrorCross from "../../../public/folder-outline-alerted.svg";
import { FC } from "react";
import { FILE_INPUT_ID } from "@/consts/elementIds";
import UploadButton from "@/components/FileSelection/UploadButton";

type Props = {
  handleFileSelection: (fileList: FileList) => void;
};
const ErrorState: FC<Props> = ({ handleFileSelection }) => {
  return (
    <>
      <Image width={50} height={50} src={ErrorCross} alt={"closed folder"} />
      <p className="text-red font-bold text-xl text-center px-4">
        Incorrect type of data selected. Please provide single file of jpeg/png
        MIME type.
      </p>
      <div className="border-red border w-full w-11/12 mt-6" />
      <input
        onChange={(event) => handleFileSelection(event.target.files!)}
        id={FILE_INPUT_ID}
        className="text-white hidden"
        type="file"
      />
      <UploadButton />
    </>
  );
};

export default ErrorState;
