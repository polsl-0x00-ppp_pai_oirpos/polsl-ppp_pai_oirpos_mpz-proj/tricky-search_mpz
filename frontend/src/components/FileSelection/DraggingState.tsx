import Image from "next/image";
import OpenFolder from "../../../public/folder-open-line.svg";

const DraggingState = () => {
  return (
    <div className="mt-12 flex flex-col items-center gap-4">
      <Image width={50} height={50} src={OpenFolder} alt={"closed folder"} />
      <p className="text-white font-bold text-xl text-center px-4">
        Drop your file here...
      </p>
    </div>
  );
};

export default DraggingState;
