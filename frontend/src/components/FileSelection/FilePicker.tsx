import { FC } from "react";
import Image from "next/image";
import FolderIcon from "../../../public/folder-line.svg";
import UploadButton from "@/components/FileSelection/UploadButton";

type Props = {
  handleFileSelection: (fileList: FileList) => void;
};
const FilePicker: FC<Props> = ({ handleFileSelection }) => {
  return (
    <>
      {" "}
      <Image width={50} height={50} src={FolderIcon} alt={"closed folder"} />
      <p className="text-white font-bold text-xl text-center">
        Drag your image here to start uploading
      </p>
      <div className="border-white border  w-full w-11/12 mt-6" />
      <input
        onChange={(event) => handleFileSelection(event.target.files!)}
        id="file-input"
        className="text-white mt-10 hidden"
        type="file"
      />
      <UploadButton />
    </>
  );
};

export default FilePicker;
