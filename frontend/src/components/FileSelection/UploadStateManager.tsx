import { UploadState } from "@/components/FileSelection/DragZoneHandler";
import { FC } from "react";
import FilePicker from "@/components/FileSelection/FilePicker";
import ErrorState from "@/components/FileSelection/ErrorState";
import DraggingState from "@/components/FileSelection/DraggingState";
import SelectedState from "@/components/FileSelection/SelectedState";

type Props = {
  uploadState: UploadState;
  handleFileSelection: (fileList: FileList) => void;
  fileName: string | null;
};

const UploadStateManager: FC<Props> = ({
  uploadState,
  handleFileSelection,
  fileName,
}) => {
  switch (uploadState) {
    case "Idle":
      return <FilePicker handleFileSelection={handleFileSelection} />;
    case "Error":
      return <ErrorState handleFileSelection={handleFileSelection} />;
    case "Dragging":
      return <DraggingState />;
    case "Selected":
      return (
        <SelectedState
          handleFileSelection={handleFileSelection}
          fileName={fileName as string}
        />
      );
  }
};

export default UploadStateManager;
