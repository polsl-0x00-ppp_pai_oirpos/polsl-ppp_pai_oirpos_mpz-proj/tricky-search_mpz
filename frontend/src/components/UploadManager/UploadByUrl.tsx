import { FC } from "react";
import { FileUrl } from "@/context/fileContext";
import { useRouter } from "next/router";
import { postImageUrl } from "@/api/queryFunctions";
import UploadURLSection from "../FileSelection/UploadURLSection";

const UploadByUrl: FC = () => {
  const { push } = useRouter();
  const callback = async (fileUrl: FileUrl) => {
    try {
      const { id } = await postImageUrl(fileUrl);
      await push(`/images/${id}`);
    } catch (err) {
      alert(
        "Error occurred while uploading image. Validate URL or try again later"
      );
    }
  };
  return (
    <div className="flex flex-col w-full justify-center items-center pt-[200px]">
      <UploadURLSection onUpload={callback} />
    </div>
  );
};
export default UploadByUrl;
