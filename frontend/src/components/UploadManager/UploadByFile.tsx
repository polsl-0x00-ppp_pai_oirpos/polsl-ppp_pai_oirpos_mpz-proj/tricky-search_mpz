import DragZoneHandler from "@/components/FileSelection/DragZoneHandler";
import { FC } from "react";
import { useFileContext } from "@/context/fileContext";
import { useRouter } from "next/router";
import { postImageFile } from "@/api/queryFunctions";

const UploadByFile: FC = () => {
  const { file } = useFileContext();
  const { push } = useRouter();
  const callback = async () => {
    try {
      // @ts-ignore
      const { id } = await postImageFile(file);
      await push(`/images/${id}`);
    } catch (err) {
      alert(
        "Error occurred while uploading image. Validate URL or try again later"
      );
    }
  };
  return (
    <div className="flex flex-col w-full justify-center items-center pt-[200px]">
      <DragZoneHandler />
      {file && (
        <button
          onClick={callback}
          className="text-dark font-bold bg-purple px-[24px] py-[12px] rounded-[12px] my-12"
        >
          GET MY YOLO!
        </button>
      )}
    </div>
  );
};
export default UploadByFile;
