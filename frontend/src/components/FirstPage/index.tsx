import Image from "next/image";
import ImageCover from "public/objectDetection.png";

const FirstPage = () => {
  return (
    <main className="flex flex-col ">
      <div className="flex flex-col items-center px-12 justify-center h-screen">
        <h2 className="text-white text-6xl ">YOLO My Image</h2>
      </div>
      <div className="bg-darker-800 gap-10 flex px-12 h-screen relative flex flex-col items-center">
        <h2 className="text-white text-5xl mx-auto mt-4">How does it work?</h2>
        <div className="flex flex-col">
          <div className="relative w-[800px] min-h-[300px] min-w-[300px]">
            <Image
              layout="fill"
              objectFit="contain"
              src={ImageCover}
              placeholder={"blur"}
              alt={"image cover"}
            />
          </div>
        </div>
        <h2 className="text-purple text-[80px] italic font-mono mt-20">
          MAGIC
        </h2>
      </div>
    </main>
  );
};
export default FirstPage;
