import Image from "next/image";
import ImageIcon from "../../../public/image-line.svg";

const Loader = () => (
  <div className="animate-pulse flex flex-col items-center">
    <Image src={ImageIcon} alt={"image loader"} width={120} height={120} />
    <h2 className="text-white text-2xl">Loading image...</h2>
  </div>
);

export default Loader;
