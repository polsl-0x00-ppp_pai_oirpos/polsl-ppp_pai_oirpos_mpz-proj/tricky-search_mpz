import Link from "next/link";

const LINKS = [
  {
    href: "/",
    text: "Homepage",
  },
  {
    href: "/upload",
    text: "Upload by File",
  },
  {
    href: "/uploadUrl",
    text: "Upload by URL",
  },
  {
    href: "/images",
    text: "Collection",
  },
];

const Navbar = () => (
  <nav className=" flex top-0 fixed w-full h-[60px] bg-orange z-50 p-4">
    <ul className="flex gap-8 items-center ml-auto">
      {LINKS.map((link) => (
        <li className="list-none">
          <Link href={link.href} key={link.href}>
            <p className="text-xl text-black sm:text-2xl text-black hover:underline">
              {link.text}
            </p>
          </Link>
        </li>
      ))}
    </ul>
  </nav>
);

export default Navbar;
