import { createContext, FC, ReactNode, useContext, useState } from "react";
import { Maybe } from "@/types";

export type FileUrl = {
  url: string;
  name: string;
};

type ContextValue = {
  file: Maybe<File | FileUrl>;
  updateFile: (file: Maybe<File | FileUrl>) => void;
};
const FileContext = createContext<Maybe<ContextValue>>(null);

type Props = {
  children: ReactNode;
};
const FileContextProvider: FC<Props> = ({ children }) => {
  const [file, setFile] = useState<Maybe<File | FileUrl>>(null);

  return (
    <FileContext.Provider value={{ updateFile: setFile, file }}>
      {children}
    </FileContext.Provider>
  );
};
export default FileContextProvider;
export const useFileContext = (): ContextValue => {
  const ctx = useContext(FileContext);
  if (!ctx) throw new Error("File context out of bounds");
  return ctx;
};
