export type Maybe<T> = T | null;
export type IdentifyImageResponse = {
  format: string;
  id: string;
  sourceUrl: string;
  imageType: string;
  categories: string[];
  imageData: string;
  name: string | null;
};
